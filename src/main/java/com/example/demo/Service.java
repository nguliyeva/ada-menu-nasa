package com.example.demo;

import com.example.demo.Domain.Meal;
import com.example.demo.Repository.MealRepository;

import java.util.HashSet;
import java.util.Set;

@org.springframework.stereotype.Service
public class Service {
private MealRepository mealRepository;

    public Service(MealRepository mealRepository) {
        this.mealRepository = mealRepository;
    }

    public Set<Meal> getMeals() {
        Set<Meal> meals = new HashSet<>();

        mealRepository.findAll().iterator().forEachRemaining(meals::add);

        return meals;
    }



}
