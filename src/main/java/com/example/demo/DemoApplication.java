package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;


@SpringBootApplication

public class DemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}


	@Bean
	WebMvcConfigurer webMvcConfigurer(){
		return new WebMvcConfigurerAdapter() {

			public void addResource(ResourceHandlerRegistry registry){
			registry.addResourceHandler("static/**")
					.addResourceLocations("classpath:/assests/","classpath:/bundles/","classpath:/js/","classpath:/","classpath:/images/","classpath:/img/","classpath:/plugins");
		}
		};
	}

}
