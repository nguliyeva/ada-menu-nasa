package com.example.demo.Repository;

import com.example.demo.Domain.Admin;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface AdminRepository extends CrudRepository<Admin,Long> {
    @Query("SELECT c FROM Admin c where c.login = :login and c.password=:pass")
    Admin checkLogin(@Param("login") String login, @Param("pass") String pass);


}
