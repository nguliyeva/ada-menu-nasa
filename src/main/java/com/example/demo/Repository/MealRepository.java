package com.example.demo.Repository;


import com.example.demo.Domain.Meal;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MealRepository extends CrudRepository<Meal,Long> {
Iterable<Meal> findAllByCanteen(String canteen);
}
