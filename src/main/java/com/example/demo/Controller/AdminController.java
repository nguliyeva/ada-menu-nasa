package com.example.demo.Controller;

import com.example.demo.Domain.Admin;
import com.example.demo.Repository.AdminRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;

@Controller
public class AdminController {
private AdminRepository adminRepository;


    public AdminController(AdminRepository adminRepository) {
        this.adminRepository = adminRepository;
    }

    @PostMapping("/dologin")
    public String doLogin(@ModelAttribute("admin") Admin admin, Model model, HttpSession session) {
        Admin admn = adminRepository.checkLogin(admin.getLogin(), admin.getPassword());
        if (admn==null){
            model.addAttribute("error",true);
            model.addAttribute("errorinfo","User not found");
            return "login";
        }
        session.setAttribute("admin", admn);
        model.addAttribute("admin", admn);

        return "/menu";
    }

    @RequestMapping("/home")
    public String divanMenu(Model model) {
        return "home_page";
    }

    @RequestMapping("/about")
    public String aboutUs(Model model) {
        return "about";
    }


}
