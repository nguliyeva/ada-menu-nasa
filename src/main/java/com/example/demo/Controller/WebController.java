package com.example.demo.Controller;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

public class WebController {

    @RequestMapping("/home")
    public String divanMenu(Model model) {
        return "home_page";
    }

    @RequestMapping("/about")
    public String aboutUs(Model model) {
        return "about";
    }


}
