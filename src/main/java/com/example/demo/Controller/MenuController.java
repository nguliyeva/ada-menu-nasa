package com.example.demo.Controller;


import com.example.demo.Domain.Meal;
import com.example.demo.Repository.MealRepository;
import com.example.demo.Service;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.Optional;

@Controller
public class MenuController {

    private MealRepository mealRepository;
    private Service service;


    public MenuController(MealRepository mealRepository, Service service) {
        this.mealRepository = mealRepository;
        this.service = service;
    }

    @PostMapping("/meal/add")
    public String saveCourse(@ModelAttribute Meal crs)  {
        mealRepository.save(crs);
        return "redirect:/menu";
    }



    @RequestMapping("/meal/update/{id}")
    public String updateMeal(@PathVariable(name="id", required = true) String id, Model model, HttpSession session) {
        Optional<Meal> meal = mealRepository.findById(Long.parseLong(id));
        model.addAttribute("meal", meal.get());
            return "form";

        }

    @RequestMapping("/divan")
    public String divanMenu(Model model) {
        Iterable<Meal> meal = mealRepository.findAllByCanteen("Divan");
        model.addAttribute("Meal", meal);
        return "divan_menu";

    }
    @RequestMapping("/via")
    public String via(Model model) {
        Iterable<Meal> meal = mealRepository.findAllByCanteen("Via");
        model.addAttribute("Meal", meal);
        return "via_presto_menu";

    }
    @RequestMapping("/book")
    public String book(Model model) {
        Iterable<Meal> meal = mealRepository.findAllByCanteen("BookBites");
        model.addAttribute("Meal", meal);
        return "book_bite_menu";

    }



    @RequestMapping("/meal/new")
    public String newMeal( ModelMap model) {
        try {
            Meal meal =new Meal();

            model.addAttribute("meal",meal);
        return "form";

        } catch (Exception e) {
            return e.toString();
        }


    }



        @RequestMapping("/menu")
    public String getDbDetils(Model model) {
     try {

            Iterable<Meal> meals = service.getMeals();

            model.addAttribute("Meal" , meals);
            return "menu";
        } catch (Exception e) {
            model.addAttribute("message", "Error in getting image");
            return "redirect:/";
        }
    }
    @RequestMapping("/meal/delete/{id}")
    public String deleteCourse(@PathVariable(name="id", required = true) String crnId, Model model) {

            Optional<Meal> meal = mealRepository.findById(Long.parseLong(crnId));
            mealRepository.delete(meal.get());

            return "redirect:/menu";

    }





}
